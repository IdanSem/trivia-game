#pragma once
#include <iostream>
#include <string>
#include <vector>

typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;

typedef struct LogoutResponse
{
	unsigned int status;
}LogoutResponse;

typedef struct  SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct ErrorResponse
{
	std::string message;
}ErrorResponse;

typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
	
	static std::vector<std::string> getNamesAndIds(const std::vector<RoomData>& room_data_vec);
}RoomData;

inline std::vector<std::string> RoomData::getNamesAndIds(const std::vector<RoomData>& room_data_vec)
{
	std::vector<std::string> string_vec;
	for (const RoomData& element: room_data_vec)
	{
		string_vec.push_back(std::to_string(element.id) + ": " + element.name);
	}
	return string_vec;
}

typedef struct GetRoomResponse
{
	std::vector<RoomData> rooms;
}GetRoomResponse;

typedef struct GetPlayersInRoomResponse
{
	std::vector<std::string> players;
}GetPlayersInRoomResponse;

typedef struct GetHighScoreResponse
{
	std::vector<std::string> statistics;
}GetHighScoreResponse; 

typedef struct GetPersonalStateResponse
{
	std::vector<std::string> statistics;
}GetPersonalStateResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
}JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;
	unsigned int room_id;
}CreateRoomResponse;