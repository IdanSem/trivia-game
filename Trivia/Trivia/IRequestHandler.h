#pragma once
#include <iostream>
#include <vector>
#include <ctime>
#include "codes.h"
#include "RequestStructs.h"

struct RequestInfo;
struct RequestResult;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo request_info) = 0;
	virtual RequestResult handleRequest(RequestInfo request_info) = 0;
};

