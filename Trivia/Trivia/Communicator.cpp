#include "Communicator.h"
#include <chrono>
#include <ctime> 
#define MAX_CHARS 1024
#define DATA_START 5
#define FIND_LEN(DATA) DATA[1] * pow(2.0, 24.0) + DATA[2] * pow(2.0, 16.0) + DATA[3] * pow(2.0, 8.0) + DATA[4] * pow(2.0, 0.0)

Communicator& Communicator::getInstance(RequestHandlerFactory* req_handler_factory)
{
	static Communicator instance(req_handler_factory);
	return instance;
}

Communicator::Communicator(RequestHandlerFactory* req_handler_factory)
{
	this->_handler_factory = req_handler_factory;
	this->m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);// creating the main socket
	// if the creation of the main socket failed, throwing an exception
	if (this->m_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}


Communicator::~Communicator()
{
	// closing the main socket
	try
	{
		closesocket(this->m_serverSocket);
		for (auto it = this->m_clients.begin(); it != this->m_clients.end(); it++) {
			delete it->second;
		}
	}
	catch (...)
	{
	}
}

void Communicator::accept()
{
	SOCKET client_socket = ::accept(this->m_serverSocket, nullptr, nullptr);
	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}
	std::cout << client_socket << " | Status: Client Accepted  (Anonymous)." << std::endl;
	auto pair = std::pair<SOCKET, IRequestHandler*>(client_socket, this->_handler_factory->createLoginRequestHandler());
	this->m_clients.insert(pair);
	std::thread client_thread(&Communicator::handleNewClient, this, client_socket);
	client_thread.detach();
}

RequestInfo Communicator::createRequestInfo(unsigned char data[]) const
{
	// find the length of the buffer
	int len = FIND_LEN(data);
	RequestInfo newRequestInfo;
	// set the id & recival time
	newRequestInfo.id = (CODES)data[0];
	newRequestInfo.receiveTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) + 3 * 60 * 60;
	// fill the buffer
	for (int i = DATA_START; i < DATA_START + len; i++)
	{
		newRequestInfo.buffer.push_back(data[i]);
	}

	return newRequestInfo;

}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(this->m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;
	
	while (true)
	{
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}

void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		
		while (true)
		{
			char data[MAX_CHARS + 1] = { NULL };
			const int res = recv(clientSocket, data, MAX_CHARS, 0);

			if (res == INVALID_SOCKET || res == SOCKET_ERROR)
			{
				std::string s = "Error while receiving from socket: ";
				s += std::to_string(clientSocket);
				throw std::exception(s.c_str());
			}

			if (isEmpty(data))
			{
				throw std::exception("Client Disconnected");
			}

			const RequestInfo currentReqInfo = createRequestInfo(reinterpret_cast<unsigned char*>(data));
			if (this->m_clients[clientSocket]->isRequestRelevant(currentReqInfo))
			{
				// handling the request
				RequestResult reqResult = this->m_clients[clientSocket]->handleRequest(currentReqInfo);
				char* response = new char[reqResult.response.size() + 1];
				// transferring the buffer to char*
				for (int i = 0; i < reqResult.response.size(); i++)
				{
					response[i] = reqResult.response[i];
				}
				// sending to the client
				if (send(clientSocket, response, reqResult.response.size(), 0) == INVALID_SOCKET)
				{
					delete[] response;
					throw std::exception ("Error while sending message to client");
				}
				// setting the next IRequestHandler
				if(reqResult.newHandler != nullptr) 
				{
					this->m_clients[clientSocket] = reqResult.newHandler;
				}
			}
		}
		closesocket(clientSocket);
	}
	catch (std::exception& e)
	{
		std::cout << "Client " << clientSocket << " Error! ";
		std::cout << "(" << e.what() << ")" << std::endl;
		
		RequestInfo requestInfo;
		requestInfo.id = CODES::LogoutCode;
		this->m_clients[clientSocket]->handleRequest(requestInfo);
		
		closesocket(clientSocket);
	}
}

void Communicator::startHandleRequest() 
{
	try
	{
		Communicator::bindAndListen();
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}
}

bool Communicator::isEmpty(char data[]) 
{
	for (int i = 0; i < MAX_CHARS; i++)
	{
		if (data[i] != '\0')
		{
			return false;
		}
	}
	return true;
}