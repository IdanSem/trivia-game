#include "JsonRequestPacketDeserializer.h"
#include "RequestStructs.h"
#include <iostream>
#include <string>


LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const std::vector<unsigned char>& buffer)
{
	std::string jsonString;
	LoginRequest loginRequest;
	for (unsigned char i : buffer)
	{
		jsonString += i;
	}
	json data = json::parse(jsonString);
	loginRequest.username = data["username"];
	loginRequest.password = data["password"];

	return loginRequest;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const std::vector<unsigned char>& buffer)
{
	std::string jsonString;
	SignupRequest signupRequest;
	json data;

	for (unsigned char i : buffer)
	{
		jsonString += i;
	}

	data = json::parse(jsonString);
	signupRequest.username = data["username"];
	signupRequest.password = data["password"];
	signupRequest.email = data["email"];
	signupRequest.birthday = data["birthday"];
	signupRequest.address = data["address"];
	signupRequest.phone = data["phone"];

	return signupRequest;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(const std::vector<unsigned char>& buffer)
{
	std::string jsonString, idNum;
	GetPlayersInRoomRequest getPlayersInRoomRequest;
	for (unsigned char i : buffer)
	{
		jsonString += i;
	}
	json data = json::parse(jsonString);
	idNum = data["id"];
	getPlayersInRoomRequest.id = std::stoi(idNum);

	return getPlayersInRoomRequest;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::vector<unsigned char>& buffer)
{
	std::string jsonString;
	JoinRoomRequest joinRoomRequest;
	for (unsigned char i : buffer)
	{
		jsonString += i;
	}
	json data = json::parse(jsonString);
	std::string id = data["id"];
	joinRoomRequest.id = std::stoi(id);

	return joinRoomRequest;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const std::vector<unsigned char>& buffer)
{
	std::string jsonString;
	CreateRoomRequest createRoomRequest;
	for (unsigned char i : buffer)
	{
		jsonString += i;
	}
	json data = json::parse(jsonString);
	createRoomRequest.room_data.name = data["name"];
	createRoomRequest.room_data.isActive = false;
	createRoomRequest.room_data.maxPlayers = std::stoi(static_cast<std::string>(data["max_players"]));
	createRoomRequest.room_data.numOfQuestionsInGame = std::stoi(static_cast<std::string>(data["num_of_questions_in_game"]));
	createRoomRequest.room_data.timePerQuestion = std::stoi(static_cast<std::string>(data["time_per_question"]));
	return createRoomRequest;
}