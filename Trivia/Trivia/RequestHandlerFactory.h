#pragma once
#include <iostream>
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "IDatabase.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "MenuRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;

class RequestHandlerFactory
{
public:
	// For Singleton
	static RequestHandlerFactory& getInstance(IDatabase* db);
	RequestHandlerFactory(RequestHandlerFactory& other) = delete;
	void operator=(const RequestHandlerFactory& other) = delete;
	
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	LoginManager* getLoginManager() const;

 private:
	// c'tor && d'tor
	RequestHandlerFactory(IDatabase* db);
	~RequestHandlerFactory();
	LoginManager* login_manager_;
	IDatabase* database_;
	RoomManager* room_manager_;
	StatisticsManager* statistic_manager_;

};

