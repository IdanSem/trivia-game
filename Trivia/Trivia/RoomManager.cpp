#include "RoomManager.h"

RoomManager::RoomManager()
= default;

RoomManager::~RoomManager()
= default;

void RoomManager::createRoom(const LoggedUser& user, RoomData& data)
{
	if (!this->rooms_.empty())
	{
		data.id = this->rooms_.rbegin()->first + 1;
	}
	else
	{
		data.id = 0;
	}
	this->rooms_.insert(std::pair<unsigned int, Room>(data.id, Room(data, user)));
}

void RoomManager::deleteRoom(const int id)
{
	this->rooms_.erase(id);
}

unsigned int RoomManager::getRoomState(const int id)
{
	return this->rooms_[id].getMetadata().isActive;
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> rooms;
	for (auto it = this->rooms_.begin(); it != this->rooms_.end(); ++it)
	{
		rooms.push_back(it->second.getMetadata());
	}
	return rooms;
}

Room& RoomManager::getRoom(unsigned int id)
{
	return this->rooms_[id];
}

RoomManager& RoomManager::getInstance()
{
	static RoomManager instance;
	return instance;
}

