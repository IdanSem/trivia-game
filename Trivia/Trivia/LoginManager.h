#pragma once
#include <vector>
#include "IDatabase.h"
#include "LoggedUser.h"

class LoginManager
{
public:
	// For Singleton
	static LoginManager& getInstance(IDatabase* database);
	LoginManager(LoginManager& other) = delete;
	void operator=(const LoginManager& other) = delete;
	
	void signup(const std::string& username, const std::string& password, const std::string& email, const std::string&
	            address, const std::string& phone, const std::string& birthday) const;
	bool login(const std::string& username, const std::string& password);
	void logout(const std::string& username);
	LoggedUser* getLoggedUserByName(std::string username);

private:
	// c'tor and d'tor
	LoginManager(IDatabase* database);
	~LoginManager();
	
	IDatabase* database_;
	std::vector<LoggedUser> logged_users_;
};

