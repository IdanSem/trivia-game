#pragma once
#include "ResponseStructs.h"
#include "LoggedUser.h"

class Room
{
public:
	Room() = default;
	Room(RoomData data, LoggedUser creating_user);
	~Room();
	void addUser(LoggedUser userToAdd);
	void RemoveUser(LoggedUser userToDelete);
	RoomData getMetadata() const;
	std::vector<std::string> getAllUsers();

private:
	RoomData metadata_;
	std::vector<LoggedUser> users_;
};

