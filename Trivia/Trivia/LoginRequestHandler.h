#pragma once
#include "IRequestHandler.h"
#include "RequestStructs.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	// c'tor && d'tor
	LoginRequestHandler(LoginManager* login_manager, RequestHandlerFactory& handler_factory);
	~LoginRequestHandler() = default;

	bool isRequestRelevant(RequestInfo request_info) override;
	RequestResult handleRequest(RequestInfo request_info) override;

private:
	RequestResult login(RequestInfo req_info);
	RequestResult signup(const RequestInfo& req_info);

	RequestHandlerFactory& handler_factory_;
	LoginManager* login_manager_;
	RoomManager* room_manager_;
	StatisticsManager* statistics_manager_;
};

