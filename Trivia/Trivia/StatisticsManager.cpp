#include "StatisticsManager.h"
#define QUESTION_MAX_TIME 30
#define QUESTIONS_PER_GAMES 10

StatisticsManager::StatisticsManager(IDatabase* db)
{
    this->database_ = db;
}


StatisticsManager& StatisticsManager::getInstance(IDatabase* db)
{
    static StatisticsManager instance(db);
    return instance;
}

std::vector<std::string> StatisticsManager::getHighScore() const
{
    std::vector<std::string> usernames = this->database_->getUsersList();
    std::vector<std::string> topScores;
    std::multimap<float, std::string> allScores;
	for (auto& username : usernames)
	{
        allScores.insert(std::pair<float, std::string>(this->getTotalScore(username), username));
	}

	// getting only the top 5
    int i = 0;
	for (auto it = allScores.rbegin(); it != allScores.rend() && i < 5; ++it, ++i)
	{
        topScores.push_back(it->second + ": " + std::to_string(it->first));
	}
	
    return topScores;
}

std::vector<std::string> StatisticsManager::getUserStatistics(const std::string& username) const
{
    std::vector<std::string> userStatisticsScore;
    userStatisticsScore.push_back("Total Answers: " + std::to_string(this->database_->getNumOfTotalAnswers(username)));
    userStatisticsScore.push_back("Correct Answers: " + std::to_string(this->database_->getNumOfCorrectAnswers(username)));
    userStatisticsScore.push_back("Total Games Played: " + std::to_string(this->database_->getNumOfPlayerGames(username)));
    userStatisticsScore.push_back("Average Answer Time: " + std::to_string(this->database_->getPlayerAverageAnswerTime(username)));
    return userStatisticsScore;
}

float StatisticsManager::getTotalScore(const std::string& username) const
{
    const int correctAnswers = this->database_->getNumOfCorrectAnswers(username);
    const int totalAnswers = this->database_->getNumOfTotalAnswers(username);
    const int totalGames = this->database_->getNumOfPlayerGames(username);
    const float averageAnswerTime = this->database_->getPlayerAverageAnswerTime(username);
    const float totalScore = (correctAnswers / static_cast<float>(totalAnswers)) * 8.0 + (static_cast<float>(totalAnswers) / (static_cast<float>(totalGames) * QUESTIONS_PER_GAMES)) * 2.0;
    return totalScore;
}

