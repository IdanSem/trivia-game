#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username)
{
    this->_username = username;
}

LoggedUser::~LoggedUser()
{
}

std::string LoggedUser::getUsername()
{
    return this->_username;
}
