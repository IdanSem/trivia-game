#include "SqliteDataBase.h"
#include <io.h>

SqliteDataBase::SqliteDataBase()
{
	this->db_ = nullptr;
	this->open();
}

SqliteDataBase::~SqliteDataBase()
{
	this->close();
}

SqliteDataBase& SqliteDataBase::getInstance()
{
	static SqliteDataBase instance;
	return instance;

}

bool SqliteDataBase::doesUserExist(const std::string& username) const
{
	bool exist = false;
	const std::string sqlStatement = "SELECT ID FROM USERS WHERE USERNAME='" + username + "';";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), is_exist_call_back, &exist, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: Searching For Username In Users Table Failed (" + std::string(__FUNCTION__) +
				").\nFull Error:\n" + std::string(errMessage)).c_str());
	}
	return exist;
}

bool SqliteDataBase::doesPasswordMatch(const std::string& username, const std::string& password) const
{
	std::string passToCheck;
	const std::string sqlStatement = "SELECT PASSWORD FROM USERS WHERE USERNAME='" + username + "';";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), is_password_match_callback, static_cast<void*>(&passToCheck), &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: Comparing User's Password Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
	return (passToCheck == password);
}

void SqliteDataBase::addNewUser(const std::string& username, const std::string& password, const std::string& email,
                                const std::string& phone, const std::string& address, const std::string& birthday) const
{
	const std::string sqlStatement = "INSERT INTO USERS(USERNAME, PASSWORD, EMAIL, PHONE, ADDRESS, BIRTHDAY) VALUES('" +
		username + "', '" + password + "', '" + email + "', '" + phone + "', '" + address + "', '" + birthday + "');";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: Adding New User To The Users Table Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
}

std::vector<std::string> SqliteDataBase::getUsersList() const
{
	const std::string sqlStatement = "SELECT USERNAME FROM USERS;";
	std::vector<std::string> users;
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), get_usernames_callback, static_cast<void*>(&users), &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: Receiving Users List From DB Table Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
	return users;
}

std::string SqliteDataBase::getQuestions() const
{
	const std::string Template = "INSERT INTO QUESTIONS (QUESTION, CORRECT_ANSWER, ANSWER_TWO, ANSWER_THREE, ANSWER_FOUR) VALUES (";
	std::string sqlStatement = "BEGIN TRANSACTION; ";
	json questions = this->generateQuestions();
	
	for (int i = 0; i < 10; i++)
	{
		sqlStatement += Template;
		sqlStatement += questions[i]["question"].dump() + ", ";
		sqlStatement += questions[i]["correct_answer"].dump() + ", ";
		for (int j = 0; j < 3; j++)
		{
			sqlStatement += questions[i]["incorrect_answers"][j].dump() + (j == 2 ? "" : ", ");
		}
		sqlStatement += "); ";
	}

	sqlStatement += "COMMIT; ";

	return sqlStatement;
}

float SqliteDataBase::getPlayerAverageAnswerTime(std::string username) const
{
	float avg = 0;
	const std::string sqlStatement = "SELECT STATISTICS.AVERAGE_TIME_ANSWER_IN_SECONDS FROM STATISTICS INNER JOIN USERS ON USERS.ID = STATISTICS.USER_ID Where USERS.USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), get_time_callback, static_cast<void*>(&avg), &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
	return avg;
}

int SqliteDataBase::getNumOfCorrectAnswers(std::string username) const
{
	int correctAnswers = 0;
	const std::string sqlStatement = "SELECT STATISTICS.NUM_OF_CORRECT_ANSWERS FROM STATISTICS INNER JOIN USERS ON USERS.ID = STATISTICS.USER_ID Where USERS.USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), get_int_callback, static_cast<void*>(&correctAnswers), &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
	return correctAnswers;
}

int SqliteDataBase::getNumOfTotalAnswers(std::string username) const
{
	int totalAnswers = 0;
	const std::string sqlStatement = "SELECT STATISTICS.NUM_OF_TOTAL_ANSWERS FROM STATISTICS INNER JOIN USERS ON USERS.ID = STATISTICS.USER_ID Where USERS.USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), get_int_callback, static_cast<void*>(&totalAnswers), &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
	return totalAnswers;
}

int SqliteDataBase::getNumOfPlayerGames(std::string username) const
{
	int totalGames = 0;
	const std::string sqlStatement = "SELECT STATISTICS.NUM_OF_PLAYER_GAMES FROM STATISTICS INNER JOIN USERS ON USERS.ID = STATISTICS.USER_ID Where USERS.USERNAME = '" + username + "';";
	char* errMessage = nullptr;
	const int res = sqlite3_exec(db_, sqlStatement.c_str(), get_int_callback, static_cast<void*>(&totalGames), &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception(
			("DB Error: (" + std::string(__FUNCTION__) + ").\nFull Error:\n" +
				std::string(errMessage)).c_str());
	}
	return totalGames;
}


bool SqliteDataBase::open()
{
	const std::string dbFileName = "Trivia.db";
	int fileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->db_);

	if (res != SQLITE_OK)
	{
		this->db_ = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	// if the db was just created, then the server will initialize it
	if (fileExist == -1)
	{
		// creating users table
		std::string sqlStatement =
			"Create Table if not exists USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, USERNAME VARCHAR(255) NOT NULL UNIQUE, \
		PASSWORD VARCHAR(255) NOT NULL, EMAIL VARCHAR(255) NOT NULL, \
		PHONE VARCHAR(255) NOT NULL, ADDRESS VARCHAR(255) NOT NULL, BIRTHDAY VARCHAR(255) NOT NULL);";
		char* errMessage = nullptr;

		res = sqlite3_exec(db_, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			throw std::exception(("DB Error: Creating Users Table Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" + errMessage).c_str());
		}

		// creating questions table
		sqlStatement =
			"Create Table if not exists QUESTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, QUESTION VARCHAR(255) NOT NULL UNIQUE, \
        CORRECT_ANSWER VARCHAR(255) NOT NULL, ANSWER_TWO VARCHAR(255) NOT NULL, \
        ANSWER_THREE VARCHAR(255) NOT NULL, ANSWER_FOUR VARCHAR(255) NOT NULL);";
		errMessage = nullptr;

		res = sqlite3_exec(db_, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			throw std::exception(("DB Error: Creating Questions Table Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" + errMessage).c_str());
		}

		// filling questions table
		sqlStatement = this->getQuestions();
		errMessage = nullptr;

		res = sqlite3_exec(db_, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			char* errMessage2 = nullptr;
			res = sqlite3_exec(db_, "ROLLBACK; ", nullptr, nullptr, &errMessage2);
			throw std::exception(("DB Error: Filling Questions Table Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" + errMessage).c_str());
		}


		sqlStatement =
			"Create Table if not exists STATISTICS (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, USER_ID INT NOT NULL, AVERAGE_TIME_ANSWER_IN_SECONDS REAL NOT NULL, \
        NUM_OF_CORRECT_ANSWERS INT NOT NULL, NUM_OF_TOTAL_ANSWERS INT NOT NULL, \
        NUM_OF_PLAYER_GAMES INT NOT NULL, FOREIGN KEY (USER_ID) REFERENCES USERS(ID));";
		errMessage = nullptr;

		res = sqlite3_exec(db_, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			throw std::exception(("DB Error: Creating STATISTICS Table Failed (" + std::string(__FUNCTION__) + ").\nFull Error:\n" + errMessage).c_str());
		}
	}
	
	return true;
}

void SqliteDataBase::close() const
{
	if (this->db_)
	{
		sqlite3_close(this->db_);
	}
}

int is_exist_call_back(void* data, int argc, char** argv, char**)
{
	// if this function is entered, there is at least 1 row with the username
	*static_cast<bool*>(data) = true;
	return 0;
}

int is_password_match_callback(void* data, int argc, char** argv, char**)
{
	*static_cast<std::string*>(data) = argv[0];
	return 0;
}

int get_time_callback(void* data, int argc, char** argv, char**)
{
	*static_cast<float*>(data) = std::stof(argv[0]);
	return 0;
}

int get_int_callback(void* data, int argc, char** argv, char**)
{
	*static_cast<int*>(data) = std::stoi(argv[0]);
	return 0;
}

int get_usernames_callback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<std::string>& users = *static_cast<std::vector<std::string>*>(data);
	for (int i = 0; i < argc; i++)
	{
		users.push_back(std::string(argv[i]));
	}
	return 0;
}
