#pragma once
#include <iostream>
#include <vector>
#include <ctime>
#include "codes.h"
#include "ResponseStructs.h"
class IRequestHandler;

typedef struct RequestInfo
{
	CODES id;
	time_t receiveTime;
	std::vector<unsigned char> buffer;
} RequestInfo;

typedef struct RequestResult
{
	std::vector<unsigned char> response;
	IRequestHandler* newHandler;
} RequestResult;

typedef struct LoginRequest {
	std::string username;
	std::string password;
} LoginRequest;

typedef struct SignupRequest {
	std::string username;
	std::string password;
	std::string email;
	std::string phone;
	std::string birthday;
	std::string address;
} SignupRequest;

typedef struct GetPlayersInRoomRequest {
	unsigned int id;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest {
	unsigned int id;
} JoinRoomRequest;

typedef struct CreateRoomRequest {
	RoomData room_data;
} CreateRoomRequest;
