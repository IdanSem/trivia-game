#pragma once

enum class CODES { LoginCode, SignupCode, ErrorCode, LogoutCode, JoinRoomCode,
			       CreateRoomCode, GetRoomsCode, GetPlayersCode, GetHighScoresCode,
				   GetPersonalStatisticsCode};
