#pragma once
#include <iostream>
#include <thread>
#include "Communicator.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	// For Singleton
    static Server& getInstance(IDatabase* db);
    Server(Server const&) = delete;
    void operator=(Server const&) = delete;
	
    void run() const;

private:
	// d'tor and d'tor
    Server(IDatabase* db);
    ~Server();

    RequestHandlerFactory* _handler_factory;
    Communicator* _communicator;
    IDatabase* _database;
};