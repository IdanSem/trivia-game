#include "MenuRequestHandler.h"
#include "RoomManager.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"

MenuRequestHandler::MenuRequestHandler(RoomManager* room_manager, LoginManager* login_manager, 
	StatisticsManager* statistics_manager, const LoggedUser& user, RequestHandlerFactory& handler_factory) : user_(user), handler_factory_(handler_factory)
{
	this->room_manager_ = room_manager;
	this->login_manager_ = login_manager;
	this->statistics_manager_ = statistics_manager;
}

MenuRequestHandler::~MenuRequestHandler()
= default;

bool MenuRequestHandler::isRequestRelevant(RequestInfo request_info)
{
	return request_info.id == CODES::CreateRoomCode || request_info.id == CODES::GetRoomsCode  ||
		   request_info.id == CODES::LogoutCode || request_info.id == CODES::GetPlayersCode   ||
		   request_info.id == CODES::JoinRoomCode || request_info.id == CODES::GetHighScoresCode;
}


RequestResult MenuRequestHandler::handleRequest(RequestInfo request_info)
{
	if (request_info.id == CODES::GetRoomsCode)
	{
		return getRoomsRequest(request_info);
	}
	else if (request_info.id == CODES::CreateRoomCode)
	{
		return createRoomRequest(request_info);
	}
	else if (request_info.id == CODES::JoinRoomCode)
	{
		return joinRoomRequest(request_info);
	}
	else if (request_info.id == CODES::GetPlayersCode)
	{
		return getPlayersInRoomRequest(request_info);
	}
	else if (request_info.id == CODES::GetPersonalStatisticsCode)
	{
		return getStatisticsRequest(request_info);
	}
	else if (request_info.id == CODES::GetHighScoresCode)
	{
		return getHighScoresRequest(request_info);
	}
	else if (request_info.id == CODES::LogoutCode)
	{
		return this->logoutRequest();
	}
	else
	{
		RequestResult requestResult;
		ErrorResponse errorResponse;
		errorResponse.message = "Fatal Error Occurred In MenuRequestHandler --> handleRequest()";
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		requestResult.newHandler = nullptr;
		return requestResult;
	}
	
}

RequestResult MenuRequestHandler::getRoomsRequest(const RequestInfo& request_info)
{
	RequestResult requestResult;
	GetRoomResponse getRoomResponse;
	getRoomResponse.rooms = this->room_manager_->getRooms();
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(getRoomResponse);
	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::createRoomRequest(const RequestInfo& request_info)
{
	RequestResult requestResult;
	CreateRoomResponse createRoomResponse;
	try
	{
		CreateRoomRequest createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request_info.buffer);
		this->room_manager_->createRoom(this->user_, createRoomRequest.room_data);
		createRoomResponse.status = 1;
		createRoomResponse.room_id = createRoomRequest.room_data.id;
	}
	catch (...)
	{
		createRoomResponse.status = 0;
		createRoomResponse.room_id = 0;
	}
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(createRoomResponse);
	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::joinRoomRequest(const RequestInfo& request_info)
{
	RequestResult requestResult;
	JoinRoomResponse joinRoomResponse;
	const JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request_info.buffer);
	try
	{
		this->room_manager_->getRoom(joinRoomRequest.id).addUser(this->user_);
		joinRoomResponse.status = 1;
	}
	catch (...)
	{
		joinRoomResponse.status = 0;
	}
	
	
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(joinRoomResponse);
	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::getPlayersInRoomRequest(const RequestInfo& request_info)
{
	RequestResult requestResult;
	const GetPlayersInRoomRequest getPlayersInRoomRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(request_info.buffer);
	try
	{
		GetPlayersInRoomResponse getPlayersInRoomResponse;
		getPlayersInRoomResponse.players = this->room_manager_->getRoom(getPlayersInRoomRequest.id).getAllUsers();
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomResponse);
	}
	catch (...)
	{
		ErrorResponse errorResponse;
		errorResponse.message = "Failed To Generate Players List";
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
	}
	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::getStatisticsRequest(const RequestInfo& request_info)
{
	RequestResult requestResult;
	try
	{
		GetPersonalStateResponse getPersonalStateResponse;
		getPersonalStateResponse.statistics = this->statistics_manager_->getUserStatistics(this->user_.getUsername());
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(getPersonalStateResponse);
	}
	catch (...)
	{
		ErrorResponse errorResponse;
		errorResponse.message = "Failed To Generate Personal Statistics";
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
	}
	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::getHighScoresRequest(const RequestInfo& request_info)
{
	RequestResult requestResult;
	try
	{
		GetHighScoreResponse getHighScoreResponse;
		getHighScoreResponse.statistics = this->statistics_manager_->getHighScore();
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(getHighScoreResponse);
	}
	catch (...)
	{
		ErrorResponse errorResponse;
		errorResponse.message = "Failed To Generate High Scores Table";
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
	}
	requestResult.newHandler = this;
	return requestResult;
}

RequestResult MenuRequestHandler::logoutRequest()
{
	RequestResult requestResult;
	LogoutResponse logoutResponse;
	try
	{
		this->login_manager_->logout(this->user_.getUsername());
		logoutResponse.status = 1;
	}
	catch (...)
	{
		logoutResponse.status = 0;
	}
	requestResult.newHandler = this;
	return requestResult;
}
