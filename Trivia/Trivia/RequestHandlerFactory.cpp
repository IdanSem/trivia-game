#include "RequestHandlerFactory.h"

RequestHandlerFactory& RequestHandlerFactory::getInstance(IDatabase* db)
{
	static RequestHandlerFactory instance(db);
	return instance;
}

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db)
{
	this->login_manager_ = &LoginManager::getInstance(db);
	this->room_manager_ = &RoomManager::getInstance();
	this->statistic_manager_ = &StatisticsManager::getInstance(db);
	this->database_ = db;
}

RequestHandlerFactory::~RequestHandlerFactory()
= default;

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this->login_manager_, *this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(const LoggedUser user)
{
	return new MenuRequestHandler(this->room_manager_, this->login_manager_, this->statistic_manager_, user , *this);
}

LoginManager* RequestHandlerFactory::getLoginManager() const
{
	return this->login_manager_;
}
