#include "IDatabase.h"

IDatabase::IDatabase()
{
}

std::size_t callback(
    const char* in,
    std::size_t size,
    std::size_t num,
    std::string* out)
{
    const std::size_t totalBytes(size * num);
    out->append(in, totalBytes);
    return totalBytes;
}

json IDatabase::generateQuestions() const
{
    const std::string url("https://opentdb.com/api.php?amount=10&category=18&type=multiple");
    CURL* curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    long httpCode(0);
    std::unique_ptr<std::string> httpData(new std::string());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

    try
    {
        // Run our HTTP GET command, capture the HTTP response code, and clean up.
        curl_easy_perform(curl);
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
        curl_easy_cleanup(curl);

        if (httpCode == 200)
        {
	        const json data = json::parse(*(httpData.get()));
        	if (data["response_code"] == 0)
        	{
                return data["results"];
        	}
            else
            {
                throw std::exception("Error Generating Trivia Questions. Failed To Get Data From The Server!");
            }
        }
        else
        {
            throw std::exception("Error Generating Trivia Questions. Failed To Get Data From The Server!");
        }
	}
	catch (...)
	{
        throw std::exception("Error Generating Trivia Questions. Failed To Get Data From The Server!");
	}


}
