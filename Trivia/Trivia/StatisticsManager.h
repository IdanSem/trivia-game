#pragma once
#include "IDatabase.h"
#include <vector>

class StatisticsManager
{
public:
	static StatisticsManager& getInstance(IDatabase* db);
	StatisticsManager(StatisticsManager& other) = delete;
	void operator=(const StatisticsManager& other) = delete;

	std::vector<std::string> getHighScore() const;
	std::vector<std::string> getUserStatistics(const std::string& username) const;
private:
	float getTotalScore(const std::string& username) const;
	explicit StatisticsManager(IDatabase* db);
	~StatisticsManager() = default;
	
	IDatabase* database_;
};

