#define CURL_STATICLIB
#pragma once
#include <iostream>
#include "curl/curl.h"
#include "json.hpp"
using json = nlohmann::json;



class IDatabase
{
public:
	IDatabase(IDatabase& other) = delete;
	void operator=(const IDatabase& other) = delete;
	
	virtual bool doesUserExist(const std::string& username) const = 0;
	virtual bool doesPasswordMatch(const std::string& username, const std::string& password) const = 0;
	virtual void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& phone, const std::string& address, const std::string& birthday) const = 0;
	virtual std::vector<std::string> getUsersList() const = 0;
	
	virtual std::string getQuestions() const = 0;
	json generateQuestions() const;

	virtual float getPlayerAverageAnswerTime(std::string username) const = 0;
	virtual int getNumOfCorrectAnswers(std::string username) const = 0;
	virtual int getNumOfTotalAnswers(std::string username) const = 0;
	virtual int getNumOfPlayerGames(std::string username) const = 0;

protected:
	IDatabase();
	virtual  ~IDatabase() = default;
};

