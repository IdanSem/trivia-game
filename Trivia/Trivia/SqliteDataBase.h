#pragma once
#include "IDatabase.h"
#include "sqlite3.h"

class SqliteDataBase : public IDatabase
{
public:
	// For Singleton
	static SqliteDataBase& getInstance();
	SqliteDataBase(SqliteDataBase& other) = delete;
	void operator=(const SqliteDataBase& other) = delete;

	bool doesUserExist(const std::string& username) const override;
	bool doesPasswordMatch(const std::string& username, const std::string& password) const override;
	void addNewUser(const std::string& username, const std::string& password, const std::string& email, const std::string& phone, const std::string& address, const std::string& birthday) const override;
	std::vector<std::string> getUsersList() const override;
	
	std::string getQuestions() const override;

	float getPlayerAverageAnswerTime(std::string username) const override;
	int getNumOfCorrectAnswers(std::string username) const override;
	int getNumOfTotalAnswers(std::string username) const override;
	int getNumOfPlayerGames(std::string username) const override;

private:
	SqliteDataBase();
	~SqliteDataBase();
	
	bool open();
	void close() const;
	sqlite3* db_;
};


// Callbacks
int is_exist_call_back(void* data, int argc, char** argv, char** azColName);
int is_password_match_callback(void* data, int argc, char** argv, char** azColName);
int get_time_callback(void* data, int argc, char** argv, char** azColName);
int get_int_callback(void* data, int argc, char** argv, char** azColName);
int get_usernames_callback(void* data, int argc, char** argv, char** azColName);