#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include "SqliteDataBase.h"

int main()
{
	try
	{
		WSAInitializer wsaInit;
		IDatabase* db = &SqliteDataBase::getInstance();
		Server* BackendServer = &Server::getInstance(db);

		BackendServer->run();
	}
	catch (std::exception& e)
	{
		std::cout << "Error occurred: " << e.what() << std::endl;
	}

	system("PAUSE");
	return 0;
}