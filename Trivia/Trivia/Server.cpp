#include "Server.h"

Server& Server::getInstance(IDatabase* db)
{
	static Server instance(db);
	return instance;
}

Server::Server(IDatabase* db)
{
	this->_handler_factory = &RequestHandlerFactory::getInstance(db);
	this->_communicator = &Communicator::getInstance(this->_handler_factory);
	this->_database = db;
}

Server::~Server() = default;

void Server::run() const
{
	std::string exit;

	std::thread t_connector(&Communicator::startHandleRequest, std::ref(*this->_communicator));
	t_connector.detach();

	while (exit != "EXIT") 
	{
		std::cin >> exit;
	}
}
