#include "JsonResponsePacketSerializer.h"

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["status"] = response.status;
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::LoginCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["status"] = response.status;
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::SignupCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["message"] = response.message;
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::ErrorCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["status"] = response.status;
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::LogoutCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["Rooms"] = joinVector(RoomData::getNamesAndIds(response.rooms));
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::GetRoomsCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["PlayersInRoom"] = joinVector(response.players);
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::GetPlayersCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["HighScores"] = joinVector(response.statistics);
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::GetHighScoresCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPersonalStateResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["UserStatistics"] = joinVector(response.statistics);
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::GetPersonalStatisticsCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["status"] = response.status;
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::JoinRoomCode);
    pushData(buffer, data);
    return buffer;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
    std::vector<unsigned char> buffer;
    json j;
    j["status"] = response.status;
    j["id"] = response.room_id;
    const std::string data = j.dump();
    pushCodeAndDataLength(buffer, data.length(), CODES::CreateRoomCode);
    pushData(buffer, data);
    return buffer;
}

void JsonResponsePacketSerializer::pushCodeAndDataLength(std::vector<unsigned char>& buffer, int dataLength, CODES code)
{
    buffer.push_back(static_cast<int>(code));
    for (int i = 3; i >= 0; i--)
    {
        buffer.push_back(dataLength >> (i * 8));
    }
}

void JsonResponsePacketSerializer::pushData(std::vector<unsigned char>& buffer, const std::string& data)
{
    for (char i : data)
    {   
        buffer.push_back(i);
    }
}

std::string JsonResponsePacketSerializer::joinVector(std::vector<std::string> vec)
{
    std::string str;

	if (vec.size() > 0)
	{
        for (int i = 0; i < (vec.size() - 1); i++)
        {
            str += vec[i];
            str += ", ";
        }
        str += vec[vec.size() - 1]; // after the last element there shouldn't be ", "
	}
	
	
    return str;
}
