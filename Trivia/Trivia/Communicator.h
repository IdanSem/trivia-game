#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include "WSAInitializer.h"
#include "Communicator.h"
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include <queue>
#include <thread>
#include <iostream>
#include <map>
#include <string>
#include <mutex>

#define PORT 8826

class Communicator
{
public:
	// For Singleton
	static Communicator& getInstance(RequestHandlerFactory* req_handler_factory);
    Communicator(Communicator& other) = delete;
    void operator=(const Communicator&) = delete;

    void startHandleRequest();

private:
    // c'tor && d'tor
    Communicator(RequestHandlerFactory* req_handler_factory);
    ~Communicator();
	
    SOCKET m_serverSocket;
    std::map<SOCKET, IRequestHandler*> m_clients;
    void bindAndListen();
    void handleNewClient(SOCKET clientSocket);
    void accept();
    RequestInfo createRequestInfo(unsigned char data[]) const;
    bool isEmpty(char data[]);
    RequestHandlerFactory* _handler_factory;
};

