#pragma once
#include "Room.h"
#include <map>

class RoomManager
{
public:
	static RoomManager& getInstance();
	RoomManager(RoomManager& other) = delete;
	void operator=(const RoomManager& other) = delete;

	void createRoom(const LoggedUser& user, RoomData& data);
	void deleteRoom(int id);
	unsigned int getRoomState(int id);
	std::vector<RoomData> getRooms();
	Room& getRoom(unsigned int id);

private:
	RoomManager();
	~RoomManager();
	std::map<unsigned int, Room> rooms_;
};