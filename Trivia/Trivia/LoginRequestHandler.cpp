#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "MenuRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

LoginRequestHandler::LoginRequestHandler(LoginManager* login_manager, RequestHandlerFactory& handler_factory) : handler_factory_(handler_factory)
{
	this->login_manager_ = login_manager;
}


bool LoginRequestHandler::isRequestRelevant(RequestInfo request_info)
{
	return request_info.id == CODES::LoginCode || request_info.id == CODES::SignupCode;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo request_info)
{
	if (request_info.id == CODES::LoginCode)
	{
		return this->login(request_info);
	}
	else if (request_info.id == CODES::SignupCode)
	{
		return this->signup(request_info);
	}
	else
	{
		// fatal error
		RequestResult reqResult;
		ErrorResponse errResponse;
		errResponse.message = "ERROR";
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(errResponse);
		reqResult.newHandler = nullptr;
		return reqResult;
	}
}

RequestResult LoginRequestHandler::login(const RequestInfo req_info)
{
	RequestResult reqResult;

	try
	{
		// making the login
		const LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(req_info.buffer);
		const bool isSucceed = this->login_manager_->login(loginRequest.username, loginRequest.password);

		// creating response
		LoginResponse loginResponse;
		if (isSucceed)
		{
			loginResponse.status = 1;
			reqResult.response = JsonResponsePacketSerializer::serializeResponse(loginResponse);
			reqResult.newHandler = this->handler_factory_.createMenuRequestHandler(LoggedUser(loginRequest.username));
		}
		else
		{
			loginResponse.status = 0;
			reqResult.response = JsonResponsePacketSerializer::serializeResponse(loginResponse);
			reqResult.newHandler = this;
		}
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;

		// if the login failed with *error* (exception), returning ErrorResponse
		ErrorResponse errResponse;
		errResponse.message = e.what();
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(errResponse);
		reqResult.newHandler = nullptr;
	}
	
	return reqResult;
}


RequestResult LoginRequestHandler::signup(const RequestInfo& req_info)
{
	RequestResult reqResult;

	try
	{
		// signing up the client
		const SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(req_info.buffer);
		this->login_manager_->signup(signupRequest.username, signupRequest.password, signupRequest.email, signupRequest.address, signupRequest.phone, signupRequest.birthday);

		// creating response
		LoginResponse loginResponse;
		loginResponse.status = 1;
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(loginResponse);
		reqResult.newHandler = this;
	}
	catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;

		// if the sign-up failed with *error* (exception), returning ErrorResponse
		ErrorResponse errResponse;
		errResponse.message = e.what();
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(errResponse);
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}
