#include "LoginManager.h"
#include <regex>

LoginManager& LoginManager::getInstance(IDatabase* database)
{
	static LoginManager instance(database);
	return instance;
}

LoginManager::LoginManager(IDatabase* database)
{
	this->database_ = database;
}

LoginManager::~LoginManager()
= default;

void LoginManager::signup(const std::string& username, const std::string& password, const std::string& email, const std::string
                          & address, const std::string& phone, const std::string& birthday) const
{
	// regex patterns
	const std::regex passwordRegex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^&*])[A-Za-z\\d!@#$%^&*]{8}$");
	const std::regex emailRegex("^[A-Za-z0-9.]+@[a-z]+.[a-z]+(\\.*[a-z]){0,}$");
	const std::regex phoneRegex("(^0((\\d{1}(-?)\\d{8})|(\\d{2}-\\d{7}))$)");
	const std::regex addressRegex("(^\\([A-Za-z\\s]+,\\s[\\d]+,\\s[A-Za-z\\s]+\\)$)");
	const std::regex birthdayRegex("^(?:(?:31(\\/|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$");
	
	if (!this->database_->doesUserExist(username))
	{
		// checking if the users' data is valid
		if (std::regex_match(password, passwordRegex))
		{
			if (std::regex_match(email, emailRegex))
			{
				if (std::regex_match(phone, phoneRegex))
				{
					if (std::regex_match(address, addressRegex))
					{
						if (std::regex_match(birthday, birthdayRegex))
						{
							// if *all* data is valid, inserting the user into the data base
							this->database_->addNewUser(username, password, email, phone, address, birthday);
						}
						else
						{
							throw std::exception(("Error in <" + username + "> registration: Birthday invalid.").c_str());
						}
					}
					else
					{
						throw std::exception(("Error in <" + username + "> registration: Address invalid.").c_str());
					}
				}
				else
				{
					throw std::exception(("Error in <" + username + "> registration: Phone number invalid.").c_str());
				}
			}
			else
			{
				throw std::exception(("Error in <" + username + "> registration: Email address invalid.").c_str());
			}
		}
		else
		{
			throw std::exception(("Error in <" + username + "> registration: Password Invalid.").c_str());
		}
	}
	else
	{
		throw std::exception(("Error in <" + username + "> registration: The username already registered.").c_str());
	}
}

bool LoginManager::login(const std::string& username, const std::string& password)
{
	// checking that the user isn't logged in
	bool found = false;
	for (int i = 0; i < this->logged_users_.size() && !found; i++)
	{
		if (this->logged_users_[i].getUsername() == username)
		{
			found = true;
		}
	}

	// checking if the password correct only if the user isn't logged
	if (!found && this->database_->doesPasswordMatch(username, password))
	{
		this->logged_users_.push_back(LoggedUser(username));
		return true;
	}

	return false;
}

void LoginManager::logout(const std::string& username)
{
	if (this->database_->doesUserExist(username))
	{
		bool found = false;
		// if the user exist, erasing the user from the logged_users_ vector
		for (int i = 0; i < this->logged_users_.size() && !found; i++)
		{
			if (this->logged_users_[i].getUsername() == username)
			{
				this->logged_users_.erase(this->logged_users_.begin() + i);
				found = true;
			}
		}
	}
}

LoggedUser* LoginManager::getLoggedUserByName(std::string username)
{
	for (int i = 0; i < this->logged_users_.size(); i++)
	{
		if (this->logged_users_[i].getUsername() == username)
		{
			return &this->logged_users_[i];
		}
	}
	return nullptr;
}
