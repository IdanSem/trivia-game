#include "Room.h"

Room::Room(const RoomData data, const LoggedUser creating_user)
{
	this->metadata_ = data;
	this->addUser(creating_user);
}

Room::~Room()
{
}

void Room::addUser(LoggedUser userToAdd)
{
	this->users_.push_back(userToAdd);
}

void Room::RemoveUser(LoggedUser userToDelete)
{
	for (auto i = 0; i < this->users_.size(); i++)
	{
		if (this->users_[i].getUsername() == userToDelete.getUsername())
		{
			this->users_.erase(this->users_.begin() + i);
			break;
		}
	}
}

RoomData Room::getMetadata() const
{
	return this->metadata_;
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> users;
	for (auto it = this->users_.begin(); it != this->users_.end(); ++it)
	{
		users.push_back(it->getUsername());
	}
	return users;
}
