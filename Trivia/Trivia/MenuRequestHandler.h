#pragma once
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RoomManager* room_manager, LoginManager* login_manager, StatisticsManager* statistics_manager,
	                   const LoggedUser& user, RequestHandlerFactory& handler_factory);
	virtual  ~MenuRequestHandler();

	bool isRequestRelevant(RequestInfo request_info) override;
	RequestResult handleRequest(RequestInfo request_info) override;
private:
	RequestResult getRoomsRequest(const RequestInfo& request_info);
	RequestResult createRoomRequest(const RequestInfo& request_info);
	RequestResult joinRoomRequest(const RequestInfo& request_info);
	RequestResult getPlayersInRoomRequest(const RequestInfo& request_info);
	RequestResult getStatisticsRequest(const RequestInfo& request_info);
	RequestResult getHighScoresRequest(const RequestInfo& request_info);
	RequestResult logoutRequest();

	LoggedUser user_;
	RoomManager* room_manager_;
	LoginManager* login_manager_;
	StatisticsManager* statistics_manager_;
	RequestHandlerFactory& handler_factory_;
};

