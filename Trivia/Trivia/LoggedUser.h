#pragma once
#include <iostream>

class LoggedUser
{
public:
	// c'tor && d'tor
	LoggedUser(std::string username);
	~LoggedUser();

	std::string getUsername();
private:
	std::string _username;
};

