#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "json.hpp"
#include "codes.h"
#include "ResponseStructs.h"
using json = nlohmann::json;

#define CASTING_TO_CHAR(X) (char)(X + 48)

class JsonResponsePacketSerializer
{
public:
	static std::vector<unsigned char> serializeResponse(LoginResponse response);
	static std::vector<unsigned char> serializeResponse(SignupResponse response);
	static std::vector<unsigned char> serializeResponse(ErrorResponse response);
	static std::vector<unsigned char> serializeResponse(LogoutResponse response);
	static std::vector<unsigned char> serializeResponse(GetRoomResponse response);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse response);
	static std::vector<unsigned char> serializeResponse(GetHighScoreResponse response);
	static std::vector<unsigned char> serializeResponse(GetPersonalStateResponse response);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse response);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse response);
private:
	static void pushCodeAndDataLength(std::vector<unsigned char>& buffer, int dataLength, CODES code);
	static void pushData(std::vector<unsigned char>& buffer, const std::string& data);
	static std::string joinVector(std::vector<std::string> vec);
};
