﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    public readonly struct RequestRoomParticipants
    {
        public readonly string id;
        public RequestRoomParticipants(string id)
        {
            this.id = id;
        }
    }
}
