﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TriviaClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            if (!Globals.connected)
            {
                Globals.com = new Communicator();
                Globals.connected = true;
            }
            if(Globals.isLoggedIn)
            {
                this.Signup_ViewBox.Visibility = Visibility.Collapsed;
                this.Login_ViewBox.Visibility = Visibility.Collapsed;
                Grid.SetRow(this.Create_Room_ViewBox, 1);
                Grid.SetRow(this.Join_Room_ViewBox, 2);
                Grid.SetRow(this.Statistics_ViewBox, 3);
                Grid.SetRow(this.Exit_ViewBox, 4);
            }
            else 
            {
                this.Create_Room_ViewBox.Visibility = Visibility.Collapsed;
                this.Join_Room_ViewBox.Visibility = Visibility.Collapsed;
                this.Statistics_ViewBox.Visibility = Visibility.Collapsed;
                Grid.SetRow(this.Signup_ViewBox, 1);
                Grid.SetRow(this.Login_ViewBox, 2);
                Grid.SetRow(this.Exit_ViewBox, 3);
            }
        }

        private void Signup_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Signup), null);
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Login), null);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            if (Globals.isLoggedIn)
            {
                try
                {
                    Globals.com.SendAndRecv("", (int)Consts.CODES.LogoutCode);
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            Application.Current.Exit();
        }

        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(CreateRoom), null);
        }

        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(JoinRoom), null);
        }


    }
}
