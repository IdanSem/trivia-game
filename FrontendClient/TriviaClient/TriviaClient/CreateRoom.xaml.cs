﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Json.Net;
using Microsoft.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TriviaClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreateRoom : Page
    {
        public CreateRoom()
        {
            this.InitializeComponent();
            
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }

        private async void Submit_Click(object sender, RoutedEventArgs e)
        {
            string name = this.name.Text.ToString();
            string max_players = this.MaxPlayersSlider.Value.ToString();
            string time_per_question = this.TimePerQuestionSlider.Value.ToString();
            string num_of_questions_in_game = this.NumOfQuestionsSlider.Value.ToString();

            if (name == "")
            {
                this.ErrMsg.Message = "Please Fill All Fields";
                this.ErrMsg.IsOpen = true;
            }
            else
            {
                this.ErrMsg.IsOpen = false;
                // LoginForm the message
                CreateRoomForm room = new CreateRoomForm(name, max_players, time_per_question, num_of_questions_in_game);
                try
                {
                    string res = JsonNet.Serialize(room);
                    dynamic json = Globals.com.SendAndRecv(res, (int)Consts.CODES.CreateRoomCode);
                    if (json["status"] == 0)
                    {
                        this.ErrMsg.Message = "Couldn't create room";
                        this.ErrMsg.IsOpen = true;
                    }
                    else
                    {
                        this.ErrMsg.Severity = InfoBarSeverity.Success;
                        this.ErrMsg.Message = "Room Creation Succeeded!";
                        this.ErrMsg.IsOpen = true;

                        string RoomID = json["id"];
                        CreateRoomData data = new CreateRoomData(name, max_players.ToString(), time_per_question.ToString(), num_of_questions_in_game.ToString(), RoomID);
                        this.Frame.Navigate(typeof(StartGamePanel), data);
                    }
                }
                catch (Exception exception)
                {
                    MessageDialog messageDialog = new MessageDialog("Fatal Error Occurred: " + exception.Message);
                    await messageDialog.ShowAsync();
                }
            }
        }

        private void MaxPlayersSlider_OnValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            this.MaxPlayerLabel.Text = $"{e.NewValue} Players";
        }

        private void NumOfQuestionsSlider_OnValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            this.NumOfQuestionsLabel.Text = $"{e.NewValue} Questions";
        }

        private void TimePerQuestionSlider_OnValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            this.TimePerQuestionLabel.Text = $"{e.NewValue} [Seconds]";
        }
    }
}
