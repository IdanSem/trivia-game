﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    readonly struct CreateRoomData
    {
        public readonly string RoomId;
        public readonly string RoomName;
        public readonly string MaxPlayers;
        public readonly string TimePerQuestion;
        public readonly string NumOfQuestions;

        public CreateRoomData(string roomName, string maxPlayers, string timePerQuestion, string numOfQuestions, string roomId)
        {
            this.RoomName = roomName;
            this.MaxPlayers = maxPlayers;
            this.TimePerQuestion = timePerQuestion;
            this.NumOfQuestions = numOfQuestions;
            this.RoomId = roomId;
        }
    }
}
