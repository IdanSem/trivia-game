﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    public readonly struct LoginForm
    {
        public readonly string username;
        public readonly string password;
        public LoginForm(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}
