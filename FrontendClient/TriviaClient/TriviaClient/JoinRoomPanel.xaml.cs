﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Json.Net;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TriviaClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class JoinRoomPanel : Page
    {
        private string _roomId;
        private string _roomName;
        public JoinRoomPanel()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            JoinRoomPanelInfo parameters = (JoinRoomPanelInfo)e.Parameter;
            this.RoomNameMessage.Text = $"You Are In Room: {parameters.name}";
            Thread t = new Thread(() => UpdateParticipantsList(parameters.id));
            t.Start();
        }


        private async void UpdateParticipantsList(string id)
        {
            while (true)
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                    {
                        this.RoomParticipants.Items.Clear();
                    }
                );
                string res = JsonNet.Serialize(new RequestRoomParticipants(id));
                dynamic json = Globals.com.SendAndRecv(res, (int)Consts.CODES.GetPlayersCode);
                string participantsString = json["PlayersInRoom"];
                string[] participants = participantsString.Split(", ");
                foreach (string participant in participants)
                {
                    await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            this.RoomParticipants.Items.Add(participant);
                        }
                    );
                }
                Thread.Sleep(3000);
            }
        }
    }
}
