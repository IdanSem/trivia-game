﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    public readonly struct JoinRoomForm
    {
        public readonly string id;
        
        public JoinRoomForm(string id)
        {
            this.id = id;
        }
    }

    public readonly struct JoinRoomPanelInfo
    {
        public readonly string id;
        public readonly string name;

        public JoinRoomPanelInfo(string id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
