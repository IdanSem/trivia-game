﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Newtonsoft.Json;

namespace TriviaClient
{
    class Communicator
    {
        private Socket sock;
        public Communicator()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];

            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 8826);
                MessageDialog messageDialog;
                // Create a TCP/IP  socket.  
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);
                    this.sock = sender;
                }
                catch (ArgumentNullException ane)
                {
                    messageDialog = new MessageDialog("ArgumentNullException: " + ane.ToString());
                    messageDialog.ShowAsync();
                }
                catch (SocketException se)
                {
                    
                    messageDialog = new MessageDialog("SocketException : " + se.ToString());
                    messageDialog.ShowAsync();
                }
                catch (Exception e)
                {
                    messageDialog = new MessageDialog("Unexpected exception : " + e.ToString());
                    messageDialog.ShowAsync();
                }

            }
            catch (Exception e)
            {
                MessageDialog messageDialog = new MessageDialog(e.ToString());
                messageDialog.ShowAsync();
            }
        }

        public dynamic SendAndRecv(string str, int code)
        {
            byte[] wholeMsg = new byte[Consts.MAX_BYTES];
            byte[] dataStr = Encoding.ASCII.GetBytes(str);
            wholeMsg[0] = Convert.ToByte(code);
            int num = 0, len = str.Length;
            for (int i = 4; i > 0; i--)
            {
                num = (int)(len / Math.Pow(2, 8*(i - 1)));
                wholeMsg[5 - i] += Convert.ToByte(num);
                len -= num;
            }
            for (int i = 0; i < dataStr.Length; i++)
            {
                wholeMsg[5 + i] = dataStr[i];
            }
            this.sock.Send(wholeMsg);
            byte[] recBytes = new byte[Consts.MAX_BYTES];
            
            this.sock.Receive(recBytes);
            string ServerMsg = "";
            int lenRecv = (int)(recBytes[1] * Math.Pow(2, 24)) + (int)(recBytes[2] * Math.Pow(2, 16)) + (int)(recBytes[3] * Math.Pow(2, 8)) + (int)(recBytes[4] * Math.Pow(2, 0));
            for (int i = Consts.DATA_START; i < Consts.DATA_START + lenRecv; i++)
            {
                    ServerMsg += System.Text.Encoding.ASCII.GetString(new[] { recBytes[i] });
            }
            return JsonConvert.DeserializeObject(ServerMsg);
        }
    }
}
