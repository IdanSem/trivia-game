﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Json.Net;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TriviaClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class JoinRoom : Page
    {
        private string selectedID;
        private string selectedName;
        public JoinRoom()
        {
            this.InitializeComponent();
            UpdateRoomNames();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }

        private void RoomNames_OnItemClick(object sender, ItemClickEventArgs e)
        {
            if (e.ClickedItem.ToString() != "")
            {
                string id = e.ClickedItem.ToString().Split(": ")[0];
                this.selectedName = e.ClickedItem.ToString().Split(": ")[1];
                selectedID = id;
                this.Join.Visibility = Visibility.Visible;

                Thread t = new Thread(() => UpdateParticipantsList(id));
                t.Start();
            }
        }

        private void UpdateRoomNames()
        {
            this.RoomNames.Items.Clear();
            dynamic json = Globals.com.SendAndRecv("", (int)Consts.CODES.GetRoomsCode); 
            string roomsString = json["Rooms"]; 
            string[] rooms = roomsString.Split(", "); 
            foreach (string room in rooms) 
            {
                this.RoomNames.Items.Add(room);
            } 
        }

        private async void UpdateParticipantsList(string id)
        {
            while (true)
            {
                await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                    {
                        this.RoomParticipants.Items.Clear();
                    }
                );
                string res = JsonNet.Serialize(new RequestRoomParticipants(id));
                dynamic json = Globals.com.SendAndRecv(res, (int)Consts.CODES.GetPlayersCode);
                string participantsString = json["PlayersInRoom"];
                string[] participants = participantsString.Split(", ");
                foreach (string participant in participants)
                {
                    await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            this.RoomParticipants.Items.Add(participant);
                        }
                    );
                }
                Thread.Sleep(3000);
            }
        }

        private void Join_Click(object sender, RoutedEventArgs e)
        {
            if (this.selectedID != null)
            {
                string res = JsonNet.Serialize(new JoinRoomForm(this.selectedID));
                dynamic json = Globals.com.SendAndRecv(res, (int)Consts.CODES.JoinRoomCode);
                if (json["status"] == 0)
                {
                    MessageDialog messageDialog = new MessageDialog("Error In Joining Room!");
                }
                else
                {
                    JoinRoomPanelInfo joinRoomPanelInfo = new JoinRoomPanelInfo(this.selectedID, this.selectedName);
                    this.Frame.Navigate(typeof(JoinRoomPanel), joinRoomPanelInfo);
                }
            }

        }

    }
}
