﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Json.Net;
using System.Text;
using Microsoft.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TriviaClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Login : Page
    {
        public Login()
        {
            this.InitializeComponent();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);
        }

        private async void Submit_Click(object sender, RoutedEventArgs e)
        {
            string username = this.Username.Text.ToString();
            string password = this.Password.Password.ToString();

            if (username == "" || password == string.Empty)
            {
                this.ErrMsg.Message = "Please Fill All Fields";
                this.ErrMsg.IsOpen = true;
            }
            else
            {
                this.ErrMsg.IsOpen = false;

                try
                {
                    // LoginForm the message
                    LoginForm loginForm = new LoginForm(username, password);
                    string res = JsonNet.Serialize(loginForm);
                    dynamic json = Globals.com.SendAndRecv(res, (int)Consts.CODES.LoginCode);
                    if (json["status"] == 0)
                    {
                        this.ErrMsg.Severity = InfoBarSeverity.Error;
                        this.ErrMsg.Message = "Username or password doesn't match";
                        this.ErrMsg.IsOpen = true;
                    }
                    else
                    {
                        Globals.isLoggedIn = true;
                        this.Frame.Navigate(typeof(MainPage), null);
                    }
                }
                catch (Exception exception)
                {
                    MessageDialog messageDialog = new MessageDialog("Fatal Error Occurred: " + exception.Message);
                    await messageDialog.ShowAsync();
                }
                
            }
        }
    }
}
