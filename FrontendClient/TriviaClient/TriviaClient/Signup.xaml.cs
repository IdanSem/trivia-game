﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Json.Net;
using Microsoft.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TriviaClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Signup : Page
    {
        public Signup()
        {
            this.InitializeComponent();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage), null);

        }

        private async void Signup_Click(object sender, RoutedEventArgs e)
        {
            string username = this.Username.Text.ToString();
            string password = this.Password.Password.ToString();
            string email = this.Email.Text.ToString();
            string phoneNumber = this.Phone.Text.ToString();
            string location = this.Location.Text.ToString();
            string birthdaySelectedDate = (this.Birthday.SelectedDate != null ? this.Birthday.SelectedDate.Value.ToString("dd/MM/yyyy") : null);

            if (username == "" || password == String.Empty || email == "" || phoneNumber ==
                "" || location == "" || birthdaySelectedDate == null)
            {
                this.ErrMsg.IsOpen = true;
            }
            else
            {
                this.ErrMsg.IsOpen = false;

                // creating the message
                SignupForm signupForm = new SignupForm(username, password, phoneNumber, email, location, birthdaySelectedDate);
                string res = JsonNet.Serialize(signupForm);
                dynamic json = Globals.com.SendAndRecv(res, (int)Consts.CODES.SignupCode);
                string err = json["message"];
                if (err != null)
                {
                    this.ErrMsg.Message = err;
                    this.ErrMsg.IsOpen = true;
                }
                else
                {
                    this.Frame.Navigate(typeof(MainPage), null);
                }
            }
        }
    }
}
