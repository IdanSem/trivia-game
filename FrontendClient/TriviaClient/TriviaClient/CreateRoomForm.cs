﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    public readonly struct CreateRoomForm
    {
        public readonly string name;
        public readonly string max_players;
        public readonly string time_per_question;
        public readonly string num_of_questions_in_game;
        public CreateRoomForm(string name, string max_players, string time_per_question, string num_of_questions_in_game)
        {
            this.name = name;
            this.max_players = max_players;
            this.time_per_question = time_per_question;
            this.num_of_questions_in_game = num_of_questions_in_game;
        }

    }
}
