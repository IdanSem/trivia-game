﻿using System;

namespace TriviaClient
{
    class Consts
    {
        
        public static int MAX_BYTES = 1024;
        public static int DATA_START = 5;
        public enum CODES { LoginCode, SignupCode, ErrorCode, LogoutCode, JoinRoomCode,
            CreateRoomCode, GetRoomsCode, GetPlayersCode, GetHighScoresCode,
            GetPersonalStatisticsCode
        };
    }
    class Globals
    {
        public static bool connected = false;
        public static bool isLoggedIn = false;
        public static Communicator com;
    }

}
