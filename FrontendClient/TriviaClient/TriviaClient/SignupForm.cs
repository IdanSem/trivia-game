﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    public readonly struct SignupForm
    {
        public readonly string username;
        public readonly string password;
        public readonly string email;
        public readonly string phone;
        public readonly string birthday;
        public readonly string address;
        public SignupForm(string username, string password, string phone, string email, string location, string birthday)
        {
            this.username = username;
            this.password = password;
            this.phone = phone;
            this.email = email;
            this.address = location;
            this.birthday = birthday;
        }
    }
}
